package com.cejv416.jdbc3;

import com.cejv416.data.FishData;
import java.util.ArrayList;

/**
 * Retrieve the data and display on the console
 *
 * @author Ken Fogel
 */
public class SimpleJDBCConsole3 {

    /**
     * Retrieve the records and display them
     *
     * @return A string containing the matching fish
     */
    private String getTheFish() {
        System.out.println("SimpleJDBCConsole3");
        FishDAO_03 fishDAO = new FishDAOImpl_03();
        ArrayList<FishData> data;
        data = fishDAO.getDietQueryRecords("Carnivore");
        StringBuilder sb = new StringBuilder();

        if (data != null) {
            for (FishData fd : data) {
                sb.append(fd.toString()).append("\n");
            }
        } else {
            sb.append("Error loading data");
        }
        return sb.toString();
    }

    /**
     * Program controller. Get the fish and display them
     */
    private void perform() {
        String data;
        data = getTheFish();
        System.out.println(data);
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        new SimpleJDBCConsole3().perform();
    }
}
