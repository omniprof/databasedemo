/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve all the records from a user query and return them as an arraylist
 * <li>3) Close the database </ul>
 *
 * @author Ken Fogel
 * @version 1.2
 */
package com.cejv416.jdbc3;

import com.cejv416.data.FishData;
import java.sql.*;
import java.util.*;

class FishDAOImpl_03 implements FishDAO_03 {

    String url = "jdbc:mysql://localhost:3306/Aquarium";
    String user = "fish";
    String password = "kfcstandard";

    public FishDAOImpl_03() {
        super();
    }

    /**
     * Retrieve all the records for fish with a specific diet
     *
     * @param whereValue The data to search for
     * @return The arraylist of matching fish
     */
    @Override
    public ArrayList<FishData> getDietQueryRecords(String whereValue) {

        ArrayList<FishData> rows = new ArrayList<>();
        String sql = "Select * from FISH where DIET = ?";
        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql);) {
            pStatement.setString(1, whereValue);
            try (
                    ResultSet resultSet = pStatement.executeQuery()) {
                while (resultSet.next()) {
                    rows.add(makeFishData(resultSet));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return rows;
    }

    /**
     * Method that extracts the data from the ResultSet and creates a bean
     *
     * @param resultSet
     * @return A FishData bean
     * @throws SQLException
     */
    private FishData makeFishData(ResultSet resultSet) throws SQLException {
        FishData fishData = new FishData(
                resultSet.getInt("ID"),
                resultSet.getString("COMMONNAME"),
                resultSet.getString("LATIN"),
                resultSet.getString("PH"),
                resultSet.getString("KH"),
                resultSet.getString("TEMP"),
                resultSet.getString("FISHSIZE"),
                resultSet.getString("SPECIESORIGIN"),
                resultSet.getString("TANKSIZE"),
                resultSet.getString("STOCKING"),
                resultSet.getString("DIET"));
        return fishData;
    }
}
