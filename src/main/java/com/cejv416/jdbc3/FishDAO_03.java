package com.cejv416.jdbc3;

import com.cejv416.data.FishData;
import java.util.ArrayList;

/**
 * Database Interface
 *
 * @author Ken Fogel
 */
public interface FishDAO_03 {

    /**
     * Retrieve all the records for fish with a specific diet
     *
     * @param whereValue The data to search for
     * @return The arraylist of matching fish
     */
    public ArrayList<FishData> getDietQueryRecords(String whereValue);
}
