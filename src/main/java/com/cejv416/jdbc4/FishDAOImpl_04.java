/**
 * This class provides the functionality to: <ul> <li>1) Open a database <li>2)
 * Retrieve a specific record based on the primary key and return it as an
 * object <li>3) Close the database </ul>
 *
 * @author Ken Fogel
 * @version 1.2
 */
package com.cejv416.jdbc4;

import com.cejv416.data.FishData;
import java.sql.*;

class FishDAOImpl_04 implements FishDAO_04 {

    String url = "jdbc:mysql://localhost:3306/AQUARIUM";
    String user = "fish";
    String password = "kfcstandard";

    /**
     * Retrieve a specific record based on the primary key
     *
     * @param primaryKey
     * @return A FishData object
     */
    @Override
    public FishData getIdQueryRecord(int primaryKey) {

        String sql = "Select * from FISH where ID = ?";
        FishData fishData = null;
        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(sql)) {
            pStatement.setLong(1, primaryKey);
            try (ResultSet resultSet = pStatement.executeQuery()) {
                if (resultSet.next()) {
                    fishData = makeFishData(resultSet);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return fishData;
    }

    /**
     * Method that extracts the data from the ResultSet and creates a bean
     *
     * @param resultSet
     * @return A FishData bean
     * @throws SQLException
     */
    private FishData makeFishData(ResultSet resultSet) throws SQLException {
        FishData fishData = new FishData(
                resultSet.getInt("ID"),
                resultSet.getString("COMMONNAME"),
                resultSet.getString("LATIN"),
                resultSet.getString("PH"),
                resultSet.getString("KH"),
                resultSet.getString("TEMP"),
                resultSet.getString("FISHSIZE"),
                resultSet.getString("SPECIESORIGIN"),
                resultSet.getString("TANKSIZE"),
                resultSet.getString("STOCKING"),
                resultSet.getString("DIET"));
        return fishData;
    }
}
