package com.cejv416.jdbc4;

import com.cejv416.data.FishData;
import java.util.Scanner;

/**
 * Retrieve the data and display on the console
 *
 * @author Ken Fogel
 */
public class SimpleJDBCConsole4 {

    private final Scanner sc;

    /**
     * Constructor
     */
    public SimpleJDBCConsole4() {
        sc = new Scanner(System.in);
    }

    /**
     * Input Ask the user for a fish ID, rejecting non numeric input
     *
     * @return The fish ID value
     */
    public int requestFishID() {
        // Declaration
        int fishID;

        // Primary Input
        System.out.println("Please enter a fish ID: ");
        // Check if there is an integer waiting in the buffer
        if (sc.hasNextInt()) {
            // There is so get it
            fishID = sc.nextInt();
        } else {
            // Invalid input so set fishID to -1
            fishID = -1;
        }
        // Clean out invalid characters
        sc.nextLine();

        // Secondary input occures if the user did not enter a value in the
        // correct range. A different prompt is used when this happens.
        while (fishID == -1) {
            System.out.println("I'm sorry, your input was invalid.");
            System.out.println("Please enter a fish ID: : ");
            // Check if there is an integer waiting in the buffer
            if (sc.hasNextInt()) {
                // There is so get it
                fishID = sc.nextInt();
            } else {
                // Invalid input so set fishID to -1
                fishID = -1;
            }
            // Clean out invalid characters
            sc.nextLine();
        }
        return fishID;
    }

    /**
     * Program controller
     */
    private void perform() {
        System.out.println("SimpleJDBCConsole4");
        FishDAO_04 fishDAO = new FishDAOImpl_04();
        FishData fishData;
        int fishID = requestFishID();
        fishData = fishDAO.getIdQueryRecord(fishID);
        System.out.println(fishData);
    }

    /**
     * Where it begins
     *
     * @param args
     */
    public static void main(String[] args) {
        new SimpleJDBCConsole4().perform();
    }

}
