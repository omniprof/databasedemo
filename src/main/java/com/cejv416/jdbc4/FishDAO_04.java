package com.cejv416.jdbc4;

import com.cejv416.data.FishData;

/**
 * Database Interface
 *
 * @author Ken
 */
public interface FishDAO_04 {

    /**
     * Retrieve a specific record based on the primary key
     *
     * @param primaryKey
     * @return A FishData object
     */
    public FishData getIdQueryRecord(int primaryKey);
}
