package com.cejv416.jdbc1;

/**
 * Retrieve the data and display on the console
 *
 * @author Ken Fogel
 */
public class SimpleJDBCConsole1 {

    /**
     * Program controller. Retrieves all the fish records in a string and
     * displays them.
     */
    private void perform() {
        System.out.println("SimpleJDBCConsole1");
        FishDAO_01 fishDAO = new FishDAOImpl_01();
        String data;
        data = fishDAO.retrieveData();
        System.out.println(data);
    }

    /**
     * Where it begins
     *
     * @param args
     */
    public static void main(String[] args) {
        SimpleJDBCConsole1 simple = new SimpleJDBCConsole1();
        simple.perform();
        System.exit(0);

    }

}
