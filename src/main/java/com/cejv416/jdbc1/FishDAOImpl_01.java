package com.cejv416.jdbc1;

import java.sql.*;

/**
 * Database access routines
 *
 * @author Ken Fogel
 *
 */
public class FishDAOImpl_01 implements FishDAO_01 {

    String url = "jdbc:mysql://localhost:3306/Aquarium";
    String user = "fish";
    String password = "kfcstandard";

    /**
     * Retrieve all the records and display them on the console Uses
     * try-with-resources so that the Connection, PreparedStatement and
     * ResultSet are closed no matter what happens
     *
     * @return A string with the records in it
     */
    @Override
    public String retrieveData() {
        // StringBuilder is a mutable String
        StringBuilder sb = new StringBuilder();
        String query = "SELECT * FROM FISH";
        try (
                Connection connection = DriverManager.getConnection(url, user, password);  
                PreparedStatement pStatement = connection.prepareStatement(query);  
                ResultSet resultSet = pStatement.executeQuery()) {
            ResultSetMetaData rsmd = resultSet.getMetaData();
            while (resultSet.next()) {
                for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                    sb.append(resultSet.getString(i)).append("\n");
                }
                sb.append("====================\n");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }

        return sb.toString();
    }
}
