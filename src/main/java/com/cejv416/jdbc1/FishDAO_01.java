package com.cejv416.jdbc1;

/**
 * Database Interface
 *
 * @author Ken Fogel
 */
public interface FishDAO_01 {

    /**
     * Retrieve all the records and display them on the console Uses
     * try-with-resources so that the Connection, PreparedStatement and
     * ResultSet are closed no matter what happens
     *
     * @return A string with the records in it
     */
    public String retrieveData();
}
