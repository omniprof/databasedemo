package com.cejv416.jdbc2;

import java.sql.*;

/**
 * Example of using the meta data to see the data type of a field in the db
 *
 * @author Ken Fogel
 *
 */
public class FishDAOImpl_02 implements FishDAO_02 {

    String url = "jdbc:mysql://localhost:3306/Aquarium";
    String user = "fish";
    String password = "kfcstandard";

    /**
     * Retrieve all the fish records including the data type
     *
     * @return A string containing all the fish records
     */
    @Override
    public String retrieveData() {
        // StringBuilder is a mutable String
        StringBuilder sb = new StringBuilder();
        String query = "SELECT * FROM FISH";
        try (
                Connection connection = DriverManager.getConnection(url, user, password);
                PreparedStatement pStatement = connection.prepareStatement(query);
                ResultSet resultSet = pStatement.executeQuery()) {
            ResultSetMetaData rsmd = resultSet.getMetaData();
            while (resultSet.next()) {
                for (int i = 1; i <= rsmd.getColumnCount(); ++i) {
                    sb.append(rsmd.getColumnClassName(i)).append("--> ").append(resultSet.getString(i)).append("\n");
                }
                sb.append("====================\n");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            System.exit(1);
        }
        return sb.toString();
    }
}
