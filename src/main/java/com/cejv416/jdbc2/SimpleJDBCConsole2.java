package com.cejv416.jdbc2;

/**
 * Retrieve the data and the data types and display on the console
 *
 * @author Ken Fogel
 */
public class SimpleJDBCConsole2 {

    /**
     * Program controller. Retrieves all the fish data
     */
    private void perform() {
        System.out.println("SimpleJDBCConsole2");
        FishDAO_02 fishDAO = new FishDAOImpl_02();
        String data;
        data = fishDAO.retrieveData();
        System.out.println(data);
    }

    /**
     * Where it all begins
     *
     * @param args
     */
    public static void main(String[] args) {
        new SimpleJDBCConsole2().perform();
    }
}
