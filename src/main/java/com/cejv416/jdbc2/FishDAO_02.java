package com.cejv416.jdbc2;

/**
 * Database Interface
 *
 * @author Ken
 */
public interface FishDAO_02 {

    /**
     * Retrieve all the fish records including the data type
     *
     * @return A string containing all the fish records
     */
    public String retrieveData();

}
